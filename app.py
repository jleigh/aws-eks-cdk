#!/usr/bin/env python3

import os

from aws_cdk import core as cdk
import aws_cdk.aws_eks as eks
import aws_cdk.aws_ec2 as ec2
import aws_cdk.aws_iam as iam

class EksCdkInfraStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # Update with your admin role
        admin_role = ""

        admin_role = iam.Role.from_role_arn(self, "UserRole", admin_role)
        app_label = {"k8s-app": "aws-node"}

        env_2 = [
                    {
                        "name": "AWS_VPC_K8S_CNI_CUSTOM_NETWORK_CFG",
                        "value": "true"
                    },
                    {
                        "name": "AWS_VPC_K8S_CNI_LOGLEVEL",
                        "value": "DEBUG"
                    },
                    {
                        "name": "AWS_VPC_CNI_NODE_PORT_SUPPORT",
                        "value": "true"
                    },
                    {
                        "name": "AWS_VPC_K8S_CNI_CONFIGURE_RPFILTER",
                        "value": "false"
                    },
                    {
                        "name": "AWS_VPC_K8S_CNI_EXTERNALSNAT",
                        "value": "false"
                    },
                    {
                        "name": "MY_NODE_NAME",
                        "value": "test-node"
                    },
                ]  

        daemonset_2_obj = {
            "apiVersion":"apps/v1",
            "kind":"DaemonSet",
            "metadata": {
                "name": "aws-node-test",
                "namespace": "kube-system-test",
                "labels": app_label
            },
            "spec": {
                "selector": {"matchLabels": app_label},
                "template": {
                    "metadata": {"labels": app_label},
                    "spec": {
                        "restartPolicy": "Always",
                        "containers": [
                            {
                                "name": "ecsdemo-nodejs",
                                "image": "brentley/ecsdemo-nodejs:latest",
                                "imagePullPolicy": "IfNotPresent",
                                "ports": [{"containerPort": 8080}],
                                "env": env_2,
                            }
                        ]
                    }
                }
            }
        }

        deployment_obj = {
            "apiVersion": "apps/v1",
            "kind": "Deployment",
            "metadata": {
                "name": "hello-kubernetes",
                "namespace": "kube-system-test",
                "labels": app_label
            },
            "spec": {
                "replicas": 3,
                "selector": {"matchLabels": app_label},
                "template": {
                    "metadata": {"labels": app_label},
                    "spec": {
                        "containers": [{
                            "name": "hello-kubernetes",
                            "image": "paulbouwer/hello-kubernetes:1.5",
                            "ports": [{"containerPort": 8080}]
                        }]
                    }
                }
            }
        }


        namespace_obj = {
            "apiVersion": "v1",
            "kind": "Namespace",
            "metadata": {
              "name": "kube-system-test"
            }
        }

        service_obj = {
            "apiVersion":"v1",
            "kind": "Service",
            "metadata": {"name": "ecsdemo-nodejs", "namespace": "kube-system-test"},
            "spec": {
                "type": "LoadBalancer",
                "ports": [{"port": 80, "protocol": "TCP"}],
                "selector": app_label
            }
        }

        kube_role_obj = {
            "apiVersion": "rbac.authorization.k8s.io/v1",
            "kind": "Role",
            "metadata": {
                "namespace": "kube-system-test",
                "name": "eks-console-dashboard-full-access-role",
            },      
            "rules": [
                {
                    "apiGroups": [""],
                    "resources": ["nodes", "namespaces", "pods"],
                    "verbs": ["get", "list"]
                },
                {
                    "apiGroups": ["apps"],
                    "resources": ["replicasets", "statefulsets", "deployments", "daemonsets"],
                    "verbs": ["get", "list"]
                },
                {
                    "apiGroups": ["batch"],
                    "resources": ["jobs"],
                    "verbs": ["get", "list"]
                },

            ]
        }

        kube_role_binding_obj = {
            "apiVersion":"rbac.authorization.k8s.io/v1",
            "kind":"RoleBinding",
            "metadata": { 
                "name": "eks-console-dashboard-full-access-binding", 
                "namespace": "kube-system-test"
            },
            "subjects": [
                {
                    "kind": "Group",
                    "name":"eks-console-dashboard-full-access-group",
                    "apiGroup": "rbac.authorization.k8s.io"
                }
            ],
            "roleRef": {
                "kind":"Role",
                "name":"eks-console-dashboard-full-access-clusterrole",
                "apiGroup": "rbac.authorization.k8s.io"
            }
        }

        cluster2 = eks.FargateCluster(self, "cluster-2",
            version=eks.KubernetesVersion.V1_21,
            cluster_name="fargate-test-4",
            masters_role=admin_role
        )
        # # attach kube api policy to the admin role.
        # aws_auth = cluster2.AwsAuth(self, "MyAwsAuth",
        #     cluster=cluster2
        # )
        cluster2.aws_auth.add_masters_role(admin_role, username=None)

        cluster2.add_nodegroup_capacity("cluster2NodeGroup",
            nodegroup_name="fargate-test-nodegroup-4",
            instance_types=[ec2.InstanceType("m5.large")],
            force_update=False,
            ami_type=eks.NodegroupAmiType.AL2_X86_64,
            desired_size=2,
            min_size=2
        )
        cluster2.aws_auth.add_role_mapping(admin_role, groups=["system:masters"])
        # Add mapping for pod execution role and node instance execuetion role

        cluster2.add_fargate_profile("profile-2",
            selectors=[{"namespace": "kube-system-test"}]
        )
        namespace = cluster2.add_manifest("cluster2NamespaceManifest", namespace_obj)

        daemonset_2 = cluster2.add_manifest("cluster2DaemonSetManifest", daemonset_2_obj)

        deployment = cluster2.add_manifest("cluster2DeploymentManifest", deployment_obj)

        service = cluster2.add_manifest("cluster2ServiceManifest", service_obj)

        kube_role = cluster2.add_manifest("cluster2RoleManifest", kube_role_obj)

        # # Bind cluster role to user
        rolebind = cluster2.add_manifest("cluster2RoleBindManifest", kube_role_binding_obj)

        daemonset_2.node.add_dependency(namespace)
        service.node.add_dependency(namespace)
        deployment.node.add_dependency(namespace)
        kube_role.node.add_dependency(namespace)
        rolebind.node.add_dependency(kube_role)


app = cdk.App()
EksCdkInfraStack(app, "EksCdkInfraStack",
    # If you don't specify 'env', this stack will be environment-agnostic.
    # Account/Region-dependent features and context lookups will not work,
    # but a single synthesized template can be deployed anywhere.

    # Uncomment the next line to specialize this stack for the AWS Account
    # and Region that are implied by the current CLI configuration.

    #env=core.Environment(account=os.getenv('CDK_DEFAULT_ACCOUNT'), region=os.getenv('CDK_DEFAULT_REGION')),

    # Uncomment the next line if you know exactly what Account and Region you
    # want to deploy the stack to. */

    #env=core.Environment(account='123456789012', region='us-east-1'),

    # For more information, see https://docs.aws.amazon.com/cdk/latest/guide/environments.html
    )

app.synth()
