
# Welcome to your CDK Python project!

This is a blank project for Python development with CDK.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

This project is set up like a standard Python project.  The initialization
process also creates a virtualenv within this project, stored under the `.venv`
directory.  To create the virtualenv it assumes that there is a `python3`
(or `python` for Windows) executable in your path with access to the `venv`
package. If for any reason the automatic creation of the virtualenv fails,
you can create the virtualenv manually.

To manually create a virtualenv on MacOS and Linux:

```
$ python -m venv .venv
```

After the init process completes and the virtualenv is created, you can use the following
step to activate your virtualenv.

```
$ source .venv/bin/activate
```

If you are a Windows platform, you would activate the virtualenv like this:

```
% .venv\Scripts\activate.bat
```

Once the virtualenv is activated, you can install the required dependencies.

```
$ pip install -r requirements.txt
```

At this point you can now synthesize the CloudFormation template for this code.

```
$ cdk synth
```

To add additional dependencies, for example other CDK libraries, just add
them to your `setup.py` file and rerun the `pip install -r requirements.txt`
command.

## Useful commands

 * `cdk ls`          list all stacks in the app
 * `cdk synth`       emits the synthesized CloudFormation template
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk docs`        open CDK documentation

Enjoy!

## Instructions

1. Once deployed, visit Cloudtrail, visit Event history in the left hand menu. Change the search type from Read-only to Event name. In the search bar, enter the event name `CreateCluster`. Click the most recent CreateCluster event. On the cluster page, scroll down to see the even record. Under session issuer, copy the `arn`. 

2. In the main search bar, search for cloudshell. Once it loads type the following:

`aws sts assume-role --role-arn <arn> --role-session-name <session_name>`

replace `<arn>` with the arn copied from step 1 and `<session_name>` with a session name eg. test. Run the command to assume the cluster creation role.


3. Once the role has been assumed. We need to update the mapped roles in the Kubeconfig. We will first update the kubeconfig context to be that of the newly deployed cluster

`aws eks update-kubeconfig --name <new_cluster> --region <region> --role-arn <creation_role_arn>`

then we need to edit the kubeconfig by running the following:

`kubectl edit configMap aws-auth -o yaml -n kube-system`

update the mappedRoles section to look like this

mapRoles: |
    - groups:
      - system:masters
      rolearn: <arn>
      username: lambda
    - groups:
      - system:masters
      rolearn: <arn>
      username: admin

Add the arns of the roles that need to access the cluster. Once the update is complete, save and quit. 

This change will allow your assumer of the role to view the workloads and nodes fro the cluster


## Troubleshooting

https://aws.amazon.com/premiumsupport/knowledge-center/eks-kubernetes-object-access-error/

